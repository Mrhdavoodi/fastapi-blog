Creating venv: python3 -m venv <venv_name>
activating virtual env: source bin/activate
executing FastApi Code: uvicorn main:app --reload
pydantic
from fastapi import Response
Status_code


## docker create network
docker network create --subnet=172.18.0.1/16 docker_network

## pgAdmin
docker run  --net docker_network --ip 172.18.0.3 --name pgadmin -p 80:80 -e 'PGADMIN_DEFAULT_EMAIL=user@domain.com' -e 'PGADMIN_DEFAULT_PASSWORD=SuperSecret' -e 'PGADMIN_CONFIG_ENHANCED_COOKIE_PROTECTION=True'  -e 'PGADMIN_CONFIG_LOGIN_BANNER="Authorised users only!"' -e 'PGADMIN_CONFIG_CONSOLE_LOG_LEVEL=10'  -d dpage/pgadmin4

## pg Container
docker run --net docker_network --ip 172.18.0.2 --name some-postgres -e POSTGRES_PASSWORD=mysecretpassword -v "postgres-data:/var/lib/postgresql/data" -d postgres


## Database library
pip install psycopg2-binary

## sqlalchemy
Create database.py and models.py

## Hash Password
pip install bcrypt
pip install passlib

## Routers in FastAPI
https://fastapi.tiangolo.com/tutorial/bigger-applications/

## OAuth2PasswordRequestForm
https://fastapi.tiangolo.com/tutorial/security/simple-oauth2/