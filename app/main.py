from sys import prefix
from typing import ContextManager, Optional, List
from fastapi import FastAPI, Response, status, HTTPException

from fastapi.encoders import jsonable_encoder
from fastapi.params import Body, Depends
from random import randrange

from passlib.utils.decor import deprecated_function
import psycopg2
from psycopg2.extras import RealDictCursor
import time
from sqlalchemy.sql.expression import false
from sqlalchemy.sql.functions import mode
from starlette.status import HTTP_201_CREATED, HTTP_204_NO_CONTENT
## SQLALCHEMY
from . import models, schemas, utils
from .database import engine, get_db
from sqlalchemy.orm import Session
from .routers import post, user, auth


models.Base.metadata.create_all(bind=engine)

app = FastAPI()

# Including Routers
app.include_router(post.router)
app.include_router(user.router)
app.include_router(auth.router)

@app.get("/") 
async def root():
    return {"message": "Hello World"}




####################################################
#     _                 _           _           _  #
#  __| | ___ _ __  _ __(_) ___ __ _| |_ ___  __| | #
# / _` |/ _ \ '_ \| '__| |/ __/ _` | __/ _ \/ _` | #  
#| (_| |  __/ |_) | |  | | (_| (_| | ||  __/ (_| | #
# \__,_|\___| .__/|_|  |_|\___\__,_|\__\___|\__,_| #
#           |_|                                    #
####################################################

## POSTGRES CONNECTION
# while True:
#     try:
#         connection = psycopg2.connect(host='172.19.0.67', 
#                                     database='fastapi',
#                                     user='postgres',
#                                     password='mysecretpassword',
#                                     cursor_factory=RealDictCursor)
#         cursor = connection.cursor()
#         print("Connected successfully to the Database")
#         break
#     except Exception as error:
#         print("Failed Connecting to Database with the error ", error)
#         time.sleep(2)
#         print("Attempting again to connect to Database")

# @app.get("/posts/{id}")
# async def get_post(id):
#     return {"Details": f"here is Post {id}"}
    


# @app.post("/posts")
# async def create_post(payload: dict = Body(...)):
#     print(payload)
#     return {"message": f"title: {payload['title']} price: {payload['price']}"}

# my_posts = [{"title": "Title of First Post", "content": "content of First Post", "index": 1}, 
#             {"title": "Title of Second post", "content": "content of Second Post", "index": 2},
#             {"title": "Title of Third post", "content": "content of Third Post", "index": 3},
#             {"title": "Title of Fourth post", "content": "content of fourth Post", "index": 4}]


# def find_post(id):
#     for p in my_posts:
#         if p["index"] == id:
#             return p

# @app.get("/posts/latest")
# async def get_post():
#     id = len(my_posts)-1
#     latest_post = my_posts[id]
#     print(latest_post)
#     return {"latest": latest_post}
