from fastapi import FastAPI, Response, status, HTTPException, APIRouter
from fastapi.params import Body, Depends
from typing import ContextManager, Optional, List
from sqlalchemy.orm import Session
from starlette.status import HTTP_201_CREATED, HTTP_204_NO_CONTENT

from .. import models, schemas, oauth2
from ..database import engine, get_db

router = APIRouter(prefix="/posts",
                   tags=["Posts"])

@router.get("", response_model=List[schemas.Post])
async def get_posts(db: Session = Depends(get_db)):
    posts = db.query(models.Post).all()
    return posts

@router.post("", status_code=HTTP_201_CREATED, response_model=schemas.Post)
async def create_post(post: schemas.PostCreate, db: Session = Depends(get_db), user_id: int = Depends(oauth2.get_current_user)):
    try:
        print(f"attempt to insert by {user_id}")
        new_post = models.Post(**post.dict()) #INEFFICIENT => (title = post.title, content = post.content, published = post.published)
        db.add(new_post)
        db.commit()
        db.refresh(new_post)
    except Exception as error:
        print(error)
    return new_post
    ## OLD CODE
    #cursor.execute(""" INSERT INTO posts (title, content, published) VALUES (%s, %s, %s) RETURNING * """,(post.title, post.content, post.published))
    #new_post = post.dict()
    #new_post['index'] = randrange(1,99999999)
    #my_posts.append(new_post)
    #new_post = cursor.fetchone()
    #connection.commit()
    ## END OLD CODE

@router.get("/{id}", response_model=schemas.Post)
async def get_post(id: int, response: Response, db: Session = Depends(get_db)):
    fetched_post = db.query(models.Post).filter(models.Post.id == id).first()
    if not fetched_post:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"Post #{id}, Not Found")
            
    return fetched_post

@router.delete("/{id}", status_code=HTTP_204_NO_CONTENT)
async def delete_post(id: int, response: Response, db: Session = Depends(get_db), user_id: int = Depends(oauth2.get_current_user)):
    fetched_post = db.query(models.Post).filter(models.Post.id == id)
    if not fetched_post.first() :
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                        detail=f"Post #{id}, Not Found")
    fetched_post.delete(synchronize_session=False)
    db.commit()
    return {"message": f"post #{id} is deleted" }    

@router.put("/{id}", response_model=schemas.Post)
async def update_post(id: int, post: schemas.PostCreate, db: Session = Depends(get_db), user_id: int = Depends(oauth2.get_current_user)):
    updated_post = db.query(models.Post).filter(models.Post.id == id)
    if not updated_post:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                        detail=f"Post #{id}, Does Not Exist ")
    #connection.commit()
    updated_post.update(post.dict(), synchronize_session=False)
    db.commit()
    return {"data": f"successfuly updated post #{id}"}
