from fastapi import FastAPI, Response, status, HTTPException, APIRouter
from fastapi.params import Body, Depends
from typing import ContextManager, Optional, List
from sqlalchemy.orm import Session
from starlette.status import HTTP_201_CREATED, HTTP_204_NO_CONTENT

from .. import models, schemas, utils
from ..database import engine, get_db

router = APIRouter(prefix="/users",
                   tags=["Users"])

@router.post("", status_code=status.HTTP_201_CREATED, response_model=schemas.UserOut)
async def create_user(user: schemas.UserCreate, db: Session = Depends(get_db)):

    hashed_password = utils.hash(user.password)
    user.password = hashed_password
    try:
        print("attempt to insert New User")
        new_user = models.User(**user.dict())
        db.add(new_user)
        db.commit()
        db.refresh(new_user)
    except Exception as error:
        print(error)
    
    return new_user

@router.get("/{id}", response_model=schemas.UserOut)
async def get_user(id: int, response: Response, db: Session = Depends(get_db)):
    fetched_user = db.query(models.User).filter(models.User.id == id).first()
    if not fetched_user:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                        detail=f"User #{id}, Does Not Exist ")
    return fetched_user